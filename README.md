#Simple Eventstore with Messages

## Preparation
```
composer install
```

## Run tests
```
php ./vendor/phpunit/phpunit/phpunit --configuration ./phpunit.xml 
```

## Usage
write some Events:
```
cd scripts
php insertEvent.php
```

readOne Event:
```
cd scripts
php getOneEventByUuid.php
```

readAll Events:
```
cd scripts
php getAllEvents.php
```

## Todo
- Make public scripts dynamic (with variables to inject)