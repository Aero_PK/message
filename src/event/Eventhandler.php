<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class Eventhandler
{
    /**
     * @var FileStorage
     */
    private $storage;
    /**
     * @var Event
     */
    private $event;

    public function __construct(Storage $storage, Event $event)
    {
        $this->storage = $storage;
        $this->event = $event;
    }

    public static function fromEvent(Storage $storage, Event $event): Eventhandler
    {
        return new self($storage, $event);
    }

    public function save(Uuid $uuid, Event $event)
    {
        $this->storage->save($uuid, $event);
    }
}
