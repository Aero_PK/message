<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

interface Event
{
    public function eventName(): string;
}