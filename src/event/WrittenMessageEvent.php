<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class WrittenMessageEvent implements Event
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Message
     */
    private $message;

    public function __construct(User $user, Message $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    public function eventName(): string
    {
        return 'writtenMessageEvent';
    }

    public function getNickanme(): string
    {
        return $this->user->getNickname();
    }

    public function getMessage(): string
    {
        return $this->message->asString();
    }
}
