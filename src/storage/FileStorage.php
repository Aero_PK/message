<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class FileStorage implements Storage
{
    /**
     * @var string
     */
    private $path;

    private function __construct(string $path)
    {
        $this->path = $path;
    }

    public static function fromPath(string $path): FileStorage
    {
        return new self($path);
    }

    public function save(Uuid $uuid, Event $event): void
    {
        file_put_contents(
            $this->path,
            serialize(
                [
                    $uuid,
                    serialize($event)
                ]
            ) . "\n",
            FILE_APPEND
        );
    }

    public function readOne(Uuid $uuid): array
    {
        $file = new \SplFileObject($this->path);
        $nextEvent = '';
        foreach ($file as $key => $line) {
            if ($file->valid()) {
                $event = unserialize($file->current());
                if ($event[0]->asString() === $uuid->asString()) {
                    if (!empty($file->current())) {
                        $nextEvent = unserialize($file->current());
                        $nextEvent[1] = unserialize($nextEvent[1]);

                    }
                }
            }
        }
        return $nextEvent;
    }

    public function readAll(): array
    {
        $file = new \SplFileObject($this->path);
        $events = [];
        foreach ($file as $key => $line) {
            if ($file->valid()) {
                if (!empty($file->current())) {
                    $nextEvent = unserialize($file->current());
                    $nextEvent[1] = unserialize($nextEvent[1]);
                    $events[] = $nextEvent[1];
                }
            }
        }
        return $events;
    }
}
