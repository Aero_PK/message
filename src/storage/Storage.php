<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

interface Storage
{
    public function save(Uuid $uuid, Event $event): void;
    public function readOne(Uuid $uuid): array;
    public function readAll(): array;
}