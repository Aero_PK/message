<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class Nickname
{
    /**
     * @var string
     */
    private $nickname;

    private function __construct(string $nickname)
    {
        $this->nickname = $nickname;
    }

    public static function fromString(string $nickname): Nickname
    {
        return new self($nickname);
    }

    public function asString(): string
    {
        return $this->nickname;
    }
}
