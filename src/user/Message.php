<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class Message
{
    /**
     * @var string
     */
    private $messageText;

    private function __construct(string $messageText)
    {
        $this->ensureMessageHasLessThan150Chars($messageText);
        $this->messageText = $messageText;
    }

    public static function fromString(string $messageText): Message
    {
        return new self($messageText);
    }

    public function asString(): string
    {
        return $this->messageText;
    }

    private function ensureMessageHasLessThan150Chars(string $messageText)
    {
        if (strlen(utf8_decode($messageText)) > 150) {
            throw new \InvalidArgumentException('only 150 characters are allowed');
        };
    }
}
