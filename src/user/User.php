<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

class User
{
    /**
     * @var Nickname
     */
    private $nickname;

    /**
     * @var bool
     */
    private $isAdmin;

    public function __construct(Nickname $nickname, bool $isAdmin)
    {
        $this->nickname = $nickname;
        $this->isAdmin = $isAdmin;
    }

    public static function fromParams(Nickname $nickname, bool $isAdmin = false): User
    {
        return new self($nickname, $isAdmin);
    }

    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function getNickname()
    {
        return $this->nickname->asString();
    }
}
