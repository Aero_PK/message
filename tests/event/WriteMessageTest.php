<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\WrittenMessageEvent
 * @uses flyeralarm\microblog\Message
 * @uses flyeralarm\microblog\User
 * @uses flyeralarm\microblog\Nickname
 * @uses flyeralarm\microblog\Uuid
 */
class WriteMessageEventTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Nickname
     */
    private $nickname;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var WrittenMessageEvent
     */
    private $writeMessageEvent;

    public function setUp()
    {
        $this->nickname = Nickname::fromString('Userone');
        $this->user = User::fromParams($this->nickname);
        $this->message = Message::fromString('my Message');

        $this->writeMessageEvent = new WrittenMessageEvent($this->user, $this->message);
    }

    public function testCreateInstanceOfWriteMessageEvent()
    {
        $this->assertInstanceOf(WrittenMessageEvent::class, $this->writeMessageEvent);
    }

    public function testReturnEventName()
    {
        $this->assertEquals('writtenMessageEvent', $this->writeMessageEvent->eventName());
    }

    public function testCanRetrieveMessage()
    {
        $this->assertEquals('my Message', $this->writeMessageEvent->getMessage());
    }

    public function testCanRetrieveNickname()
    {
        $this->assertEquals('Userone', $this->writeMessageEvent->getNickanme());
    }
}