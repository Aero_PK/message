<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\EventHandler
 * @uses flyeralarm\microblog\FileStorage
 * @uses flyeralarm\microblog\WrittenMessageEvent
 * @uses flyeralarm\microblog\Message
 * @uses flyeralarm\microblog\User
 * @uses flyeralarm\microblog\Nickname
 * @uses flyeralarm\microblog\Uuid
 */
class EventHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Nickname
     */
    private $nickname;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var WrittenMessageEvent
     */
    private $writeMessageEvent;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var Eventhandler
     */
    private $eventhandler;

    /**
     * @var Uuid
     */
    private $uuid;

    public function setUp()
    {
        if (!file_exists(__DIR__ . '/../data/storage')) {
            touch(__DIR__ . '/../data/storage');
        }

        $this->nickname = Nickname::fromString('Userone');
        $this->user = User::fromParams($this->nickname);
        $this->message = Message::fromString('my Message');
        $this->writeMessageEvent = new WrittenMessageEvent($this->user, $this->message);
        $this->uuid = Uuid::fromString('5ad3691c-6706-421e-8c1d-47e77e9e176d');

        $this->storage = FileStorage::fromPath(__DIR__ . '/../data/storage');

        $this->eventhandler = Eventhandler::fromEvent($this->storage, $this->writeMessageEvent);
    }

    public function testCreateInstanceOfEventHandler()
    {
        $this->assertInstanceOf(EventHandler::class, $this->eventhandler);
    }

    public function testSaveWriteMessageEventToFile()
    {
        $this->eventhandler->save($this->uuid, $this->writeMessageEvent);
        $this->assertFileExists(__DIR__ . '/../data/storage');
        #ToDo assert for objects in file
    }

    public function tearDown()
    {
        if (file_exists(__DIR__ . '/../data/storage')) {
            unlink(__DIR__ . '/../data/storage');
        }
    }
}