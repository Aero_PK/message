<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\Message
 */
class MessageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Message
     */
    private $message;

    public function setUp()
    {
        $text = 'my Message';
        $this->message = Message::fromString($text);
    }

    public function testCreateInstanceOfMessage()
    {
        $this->assertInstanceOf(Message::class, $this->message);
    }

    public function testThrowExceptionWhenMessageIsLargerThan150Chars()
    {
        $this->expectException('\InvalidArgumentException');
        $this->expectExceptionMessage("only 150 characters are allowed");

        $messageText = 'This message has way more characters than allowed. So the error will be displayed, that only '.
            'less than 150 characters are allowed. And here stands somthing to fill the space.';
        Message::fromString($messageText);
    }

    public function testCanGetMessageAsString()
    {
        $this->assertEquals('my Message', $this->message->asString());
    }
}
