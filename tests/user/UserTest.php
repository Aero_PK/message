<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\User
 * @uses flyeralarm\microblog\Nickname
 */
class UserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var User
     */
    private $adminUser;

    public function setUp()
    {
        $nickname = Nickname::fromString('Userone');
        $this->user = User::fromParams($nickname);
        $this->adminUser = User::fromParams($nickname, true);
    }

    public function testCreateInstanceOfUser()
    {
        $this->assertInstanceOf(User::class, $this->user);
    }

    public function testIsUserAdminPrivileged()
    {
        $this->assertTrue($this->adminUser->isAdmin());
    }

    public function testCanRetrieveNicknameFromUser()
    {
        $this->assertEquals('Userone', $this->user->getNickname());
    }
}
