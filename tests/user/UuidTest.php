<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers \flyeralarm\microblog\Uuid
 */
class UuidTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Uuid
     */
    private $uuid;

    public function setUp()
    {
        $this->uuid = Uuid::generate();
    }

    public function testIsGeneratedUuidEqualToUuidGeneratedFromGivenString()
    {
        $this->assertEquals($this->uuid, Uuid::fromString($this->uuid->asString()));
    }

    public function testCantCreateUuidWithInvalidString()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('UUID has wrong format');

        Uuid::fromString('hierstehteinfalscherstring');
    }
}
