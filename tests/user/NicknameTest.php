<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\Nickname
 */
class NicknameTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Nickname
     */
    private $nickname;

    public function setUp()
    {
        $this->nickname = Nickname::fromString('Userone');
    }

    public function testCreateInstanceOfUser()
    {
        $this->assertInstanceOf(Nickname::class, $this->nickname);
    }

    public function testCanRetrieveNicknameAsString()
    {
        $this->assertTrue(is_string($this->nickname->asString()));
    }

    public function testCanRetrieveNickname()
    {
        $this->assertEquals('Userone', $this->nickname->asString());
    }
}
