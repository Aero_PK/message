<?php
declare(strict_types=1);

namespace flyeralarm\microblog;

/**
 * @covers flyeralarm\microblog\FileStorage
 * @uses flyeralarm\microblog\WrittenMessageEvent
 * @uses flyeralarm\microblog\Message
 * @uses flyeralarm\microblog\User
 * @uses flyeralarm\microblog\Nickname
 * @uses flyeralarm\microblog\Uuid
 */
class StorageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FileStorage
     */
    private $storage;

    /**
     * @var Nickname
     */
    private $nickname;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var WrittenMessageEvent
     */
    private $writeMessageEvent;

    /**
     * @var Uuid
     */
    private $uuid;

    public function setUp()
    {
        if (!file_exists(__DIR__ . '/../data/storage')) {
            touch(__DIR__ . '/../data/storage');
        }

        $this->storage = FileStorage::fromPath(__DIR__ . '/../data/storage');

        $this->nickname = Nickname::fromString('Userone');
        $this->user = User::fromParams($this->nickname);
        $this->message = Message::fromString('my Message');
        $this->uuid = Uuid::fromString('5ad3691c-6706-421e-8c1d-47e77e9e176d');

        $this->writeMessageEvent = new WrittenMessageEvent($this->user, $this->message);
    }
    public function testCreateInstanceOfStorage()
    {
        $this->assertInstanceOf(FileStorage::class, $this->storage);
    }

    public function testSaveWriteMessageEventToFile()
    {
        $this->storage->save($this->uuid, $this->writeMessageEvent);
        $this->assertFileExists(__DIR__ . '/../data/storage');
        #ToDo assert some cool regex
    }

    public function testCanReadOneEventWithUuidFromGivenUuid()
    {
        $this->storage->save($this->uuid, $this->writeMessageEvent);
        $eventFromEventstore = $this->storage->readOne($this->uuid);

        $this->assertInstanceOf(Uuid::class, $eventFromEventstore[0]);
        $this->assertInstanceOf(WrittenMessageEvent::class, $eventFromEventstore[1]);
    }

    public function testCanReadAllEventsFromStorage()
    {
        $this->storage->save($this->uuid, $this->writeMessageEvent);
        $this->storage->save($this->uuid, $this->writeMessageEvent);
        $this->storage->save($this->uuid, $this->writeMessageEvent);

        $expectedEvents = [
            $this->writeMessageEvent,
            $this->writeMessageEvent,
            $this->writeMessageEvent,
        ];

        $eventsFromEventstore = $this->storage->readAll();

        $this->assertEquals($expectedEvents, $eventsFromEventstore);
    }

    public function tearDown()
    {
        if (file_exists(__DIR__ . '/../data/storage')) {
            unlink(__DIR__ . '/../data/storage');
        }
    }
}
