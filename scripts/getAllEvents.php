<?php

require_once(__DIR__ .'/../vendor/autoload.php');

$storage = flyeralarm\microblog\FileStorage::fromPath(__DIR__.'/../src/data/storage');
$uuid = flyeralarm\microblog\Uuid::fromString('5ad3691c-6706-421e-8c1d-47e77e9e176d');
$toPrint = '';

$events = $storage->readAll();

foreach ($events as $event){
    $toPrint .=
        ' User: '.$event->getNickanme()
        .' says: '. $event->getMessage()
        .PHP_EOL;
}
print_r(
    $toPrint
);