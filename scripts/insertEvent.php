<?php

require_once(__DIR__ .'/../vendor/autoload.php');

#Store
$storage = flyeralarm\microblog\FileStorage::fromPath(__DIR__.'/../src/data/storage');

#Event
$nickname = flyeralarm\microblog\Nickname::fromString('Userone');
$user = flyeralarm\microblog\User::fromParams($nickname);
$message = flyeralarm\microblog\Message::fromString('my Message');
$uuid = flyeralarm\microblog\Uuid::generate();

#Action
$writeMessageEvent = new flyeralarm\microblog\WrittenMessageEvent($user, $message);

#Save
$storage->save($uuid, $writeMessageEvent);