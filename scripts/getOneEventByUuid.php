<?php

require_once(__DIR__ .'/../vendor/autoload.php');

$storage = flyeralarm\microblog\FileStorage::fromPath(__DIR__.'/../src/data/storage');
$uuid = flyeralarm\microblog\Uuid::fromString('7398943b-d358-42cc-8af9-dcf0c604363e');

$event = $storage->readOne($uuid);

print_r(
    'In Event: '.$event[0]->asString()
    .' User: '.$event[1]->getNickanme()
    .' says: '. $event[1]->getMessage()
    .PHP_EOL
);